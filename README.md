__Archived:__ The contents of this repository have been merged
into [dune-website](https://gitlab.dune-project.org/infrastructure/dune-website/).

Helper scripts for release management
=====================================

Getting started
---------------

- set up `releases/M.m` branches in all core modules
- update the version in the `dune.module` in the master branch of all core
  modules
- update the `CHANGELOG.md` file (replace the line `Master (will become release 2.9)` by `Release 2.9` for example).
- __IMPORTANT__: before going any further install `git-lfs`
  on your system otherwise uploading the release (candidates)
  will mess up `dune-website` completely. Try running
  `git lfs` to see if you have what is needed...
- Update the copyright holder lists in `LICENCE.md`.
  For this we provide a script in this repository:
  `dune-git-copyright`. First run

    ~~~
    ./dune-common/bin/dunecontrol exec ../releases/dune-git-copyright
    ~~~

    and check the new `LICENCE.md` files in all the repos. Check if
    there are any alias hanging around or other weird looking authors.
    These can be fixed by adding to the `aliases` dictionary
    in `dune-git-copyright`. That list should be used for the core
    modules only. For some downstream module you can add a file
    `.authorsalias` to the repo with a list of the form
    `alias , original name` (or for example `dunebot , None` to
    ignore an author).

    Finally, commit the `LICENCE.md` files that have changed.

Release (candidates)
--------------------

### 1. Generating tar balls and tags

We're assuming that this repo and the git repositories of all
relevant dune modules (checked out to the correct release branch)
are downloaded into a common working directory. In this directory call

~~~
./dune-common/bin/dunecontrol exec ../releases/make-release revision [rc number]
~~~

The first parameter corresponds to the required revision and the optional
argument can be used to number release candidates. So assume that the
version in the `dune.module` file is `M.m-git` then the version of the
release will be `M.m.revision` (or `M.m.revision-rcZ` where `Z` is the
release candidate passed as second argument).

Of course `--only` and `--module` can be used to generate release tags and
tar balls for specific modules only.

Executing `make-release` will generate the `LICINCE` file, generate a tag
(locally) in each module, and produce the tar balls for each module
putting them into the common work directory. In addition a combined
`CHANGELOG.md` file is also produced.

***Note***:
the tag will be signed depending on your key you might need to add a
`-u` option to the `git tag` command in `releases/dune-release` script.
If you're unhappy with things you can remove the generated tag (well
actually this command will erase all local tags not present on the remote) by calling:

~~~
./dune-common/bin/dunecontrol git fetch --prune origin "+refs/tags/*:refs/tags/*"
~~~

**Note**: the `LICENSE.md` might also have been updated if
required. The commit has been carried out locally so the generated
tag includes changes to the authors list in the licence file.
As pointed out at the beginning it might be a good idea to first update
the licence files in the release branches by hand.

### 2. Uploading

When you're sure everything is as it should be:

- Add the `CHANGELOG.md` file to
  `dune-website/content/releases/M.m.revision.md`
  Don't forget to add the metadata-header for the page, along the lines of
```
+++
date = "2018-02-10T16:40:00+01:00"
version = "2.6.0rc2"
major_version = "2"
minor_version = "6"
patch_version = "0rc2"
modules = ["dune-common", "dune-istl", "dune-geometry", "dune-grid", "dune-grid-howto", "dune-localfunctions"]
signed = 1
title = "Dune 2.6.0rc2"
doxygen_branch = ["v2.6.0rc2"]
doxygen_url = ["/doxygen/2.6.0rc2"]
doxygen_name = ["Dune Core Modules"]
doxygen_version = ["2.6.0rc2"]
[menu]
  [menu.main]
    parent = "releases"
    weight = -1
+++
```
- Copy the tar balls to
  `dune-website/static/download/M.m.revision` (possibly adding
  the `rcZ` for the release candidate).
  And push (***remember that you need `git-lfs`***)
- As noted the `LICENCE.md` file can have changed. They
  will have been locally committed but will need pushing.
  Of course you can again use `dunecontrol` for this...
- Push the tags using `git push origin vM.m.Z` (don't forget the `rcZ`
  if needed) in all dune repositories (use `dunecontrol` again).

### 3. Keep fingers crossed....
